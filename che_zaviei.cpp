#include <iostream>
#include <math.h>
#define PI 3.14159265
using namespace std;

float get_angle(int x1, int y1, int x2, int y2){
	float x_diff = x2 - x1;
	float y_diff = y2 - y1;
	// cout << "diff x[" << x_diff << "], y[" << y_diff <<"]\n";
	if(x_diff == 0)
		return 90;
	float param = y_diff / x_diff;
	if(x_diff > 0)
		return atan (param) * 180.0 / PI;
	else if(y_diff > 0)
		return atan (param) * 180.0 / PI + 180;
	else
		return atan (param) * 180.0 / PI - 180;

}

float rotation_angle(int xh, int yh, int xm, int ym, float m_an)
{
	float delta_an = get_angle(xh,yh, xm, ym);
	return delta_an - m_an;
}

// int main(){
// 	cout << get_angle(0,0, 5,5) << endl;
// 	cout << get_angle(0,5,5,0 ) << endl;
// 	cout << rotation_angle(4,4,7,8,45)<<endl; // 45- 37 = 8
// }
