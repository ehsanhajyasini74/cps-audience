#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <aruco/aruco.h>
#include <aruco/cvdrawingutils.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <chrono>
#include "che_zaviei.cpp"
using namespace cv;
using namespace std;
using namespace aruco;
// using namespace std::chrono_literals;

/************** ARUCO *******************/
string TheInputVideo;
string TheIntrinsicFile;
float TheMarkerSize = 0.115;
MarkerDetector MDetector;
vector< Marker > TheMarkers;
Mat TheInputImage, TheInputImageCopy;
CameraParameters TheCameraParameters;
void cvTackBarEvents(int pos, void *);
bool readCameraParameters(string TheIntrinsicFile, CameraParameters &CP, Size size);
/*---------------------------------------*/
pair< double, double > AvrgTime(0, 0); // determines the average time required for detection
double ThresParam1, ThresParam2;
int iThresParam1, iThresParam2;
int waitTime = 10;
/******************POS DETECT**********************/
VideoCapture cap;
int y_cut =800;
int x_start = 0;
int x_end = 1280;
/***************** CURL *****************/
int curl_ang = 0;
bool curl_should_run = false;
bool curl_kill = false;
int theta0 = 0;
Marker src_wrap_marker;
Mat bg_orig, gray_bg;

bool open_video() {
    cout << "opening RTSP"<<endl;
    //cap.open("rtsp://192.168.0.100/live4.sdp");
    cap.open("live.mp4");
    return cap.isOpened();
}

bool initialize() {
    TheIntrinsicFile = "ip_calib.yml";
    // check video is open
    if (!open_video()) {
        cerr << "Could not open video" << endl;
        return false;
    }
    cap >> TheInputImage;
    // read camera parameters if passed
    if (TheIntrinsicFile != "") {
        cout << "here!" << endl;
        TheCameraParameters.readFromXMLFile(TheIntrinsicFile);
        TheCameraParameters.resize(TheInputImage.size());
        cout << "camera is valid "  << TheCameraParameters.isValid() << endl;
        if (!TheCameraParameters.isValid()) {
            cerr << "cam param is not valid" << endl;
            return false;
        }
    }
    namedWindow("countours", 1);
    namedWindow("in", 1);
    return true;
}

void init_aruco(){
    MDetector.setThresholdParams(4,12);
    MDetector.setThresholdParamRange(2, 0);
}

inline void curl(int ang) {
     stringstream ss;
     ss << "curl -X GET \"http://192.168.0.101/on?" << int(180 -ang + theta0)  << "\"";
     system(ss.str().c_str());
}

void curl_runner() {
    while(!curl_kill) {
     if(curl_ang<=180 && curl_ang >= 0 && curl_should_run){
         curl(curl_ang);
         curl_should_run = false;
        } 
    this_thread::sleep_for(std::chrono::milliseconds(300));
    }  
}




Mat wrap_prespective(Mat image){//, Marker src) {
    float boardX = 600;
    float boardY = 600;
    float boardWidth = 50;
    float boardHeight = 50;
    vector<Point2f> destinationCorners;
    destinationCorners.push_back(Point2f(boardX+boardWidth, boardY));
    destinationCorners.push_back(Point2f(boardX+boardWidth, boardY+boardHeight));
    destinationCorners.push_back(Point2f(boardX, boardY+boardHeight));
    destinationCorners.push_back(Point2f(boardX, boardY));

    Mat h = getPerspectiveTransform(src_wrap_marker, destinationCorners);
    Mat bigImage(image.size() , image.type(), Scalar(0, 50, 50));
    warpPerspective(image, bigImage, h, bigImage.size());   
    return bigImage;
}

void init_theta0()
{
    cap.read(TheInputImage);

    // Rect crop_rect(x_start,0, x_end - x_start, y_cut);
    // gray_bg = gray_bg(crop_rect);

    MDetector.detect(TheInputImage, TheMarkers, TheCameraParameters, TheMarkerSize);
    for (unsigned int i = 0; i < TheMarkers.size(); i++) {
        Marker curr_m = TheMarkers[i];
        src_wrap_marker = curr_m;
        if (curr_m.getArea() < 3000)
            continue;
        theta0 = 0; //get_angle(curr_m[1].x, curr_m[1].y, curr_m[0].x, curr_m[0].y); 
        cout << "1,0 " << get_angle(curr_m[1].x, curr_m[1].y, curr_m[0].x, curr_m[0].y) << endl;
        cout << "2,3 " << get_angle(curr_m[2].x, curr_m[2].y, curr_m[3].x, curr_m[3].y) << endl;
        cout << "1,2 " << get_angle(curr_m[1].x, curr_m[1].y, curr_m[2].x, curr_m[2].y) << endl;
        cout << "3,0 " << get_angle(curr_m[3].x, curr_m[3].y, curr_m[0].x, curr_m[0].y) << endl;
    }
    bg_orig = wrap_prespective(TheInputImage);
    cvtColor(bg_orig, gray_bg, COLOR_BGR2GRAY);
}


int main(int argc, char const *argv[])
{
    //curl(180);
    //waitKey(3000);
    if(!initialize())
        return -1;
    cerr <<"init done" << endl;
    // read first image to get the dimensions
    //bg_orig = imread("1.png");
    // cap.read(bg_orig);
    theta0 = 0;
    

    Mat frame, gray_frame, gray_diff, thresh_mat, im;
    vector<vector<Point> >contours;
    vector<Vec4i> hierarchy;

    init_aruco();
    cerr << "init  aruco done" << endl;

    init_theta0();
    cerr << "init theta done " << endl;

    int index = 0;
    //thread c(curl_runner);

    while(true)
    {
        try{
            

        cap.read(TheInputImage);
        //TheInputImage.copyTo(frame);
        Mat wrap = wrap_prespective(TheInputImage);
        imshow("wrap",wrap );
        wrap.copyTo(frame);
        wrap.copyTo(TheInputImage);
        //cap.read(frame);
        index++; // number of images captured
        double tick = (double)getTickCount(); // for checking the speed
        // Detection of markers in the image passed
        MDetector.detect(frame, TheMarkers, TheCameraParameters, TheMarkerSize);
        cout << "detected Marker count " << TheMarkers.size() << endl;
        // chekc the speed by calculating the mean speed of all iterations
        // AvrgTime.first += ((double)getTickCount() - tick) / getTickFrequency();
        // AvrgTime.second++;
        // cout << "\r[M] Time detection=" << 1000 * AvrgTime.first / AvrgTime.second << " milliseconds nmarkers=" << TheMarkers.size() << endl;

        // print marker info and draw the markers in image
        frame.copyTo(TheInputImageCopy);
        // double tick = (double)getTickCount(); // for checking the speed


        cvtColor(frame, gray_frame, COLOR_BGR2GRAY);
        //gray_frame = gray_frame(crop_rect);
        absdiff(gray_frame, gray_bg, gray_diff);
        threshold(gray_diff, thresh_mat, 60,255,THRESH_BINARY);
        
        findContours (thresh_mat, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
        cvtColor(thresh_mat, im, COLOR_GRAY2RGB);
        drawContours( im, contours, -1, Scalar(255,255,255), -1, 8, hierarchy ); // first -1 => all contours
        int cx, cy;
        vector<Point> max_contour;
        if(contours.size())
        {
            max_contour = contours[0];
            double max_size = contourArea(contours[0]);
            for (vector<vector<Point> >::iterator c = contours.begin(); c != contours.end(); ++c)
            {
                if(contourArea(*c) > max_size)
                {
                    max_contour = *c;
                    max_size = contourArea(*c);
                }
            }
            Moments M = moments(max_contour);
            cx = int(M.m10/M.m00);
            cy = int(M.m01/M.m00);
            //filter
            // cout<<"max area = "<<max_size<<" (x,y) = "<<cx<<" "<<cy<<endl; // , "filter" , int(my_filter.x[0])
            if(contourArea(max_contour)>7000){
                circle(im,Point(cx,cy),20,Scalar(0,0,255),-1); // -1 => FILLED circle
                circle(TheInputImageCopy,Point(cx,cy),20,Scalar(0,0,255),-1); // -1 => FILLED circle
                cout<<"Human detected! : ";
                cout<<"max area = "<<max_size<<" (x,y) = "<<cx<<" "<<cy<<endl; // , "filter" , int(my_filter.x[0])
            }
            else{


                circle(im,Point(cx,cy),20,Scalar(0,255,0),-1); // -1 => FILLED circle 
            }

        }
        
        
        // for( int idx = 0; idx >= 0; idx = hierarchy[idx][0] )
        //     drawContours( dst, contours, idx, Scalar(255,255,255), -1, 8, hierarchy );
        
        
        for (unsigned int i = 0; i < TheMarkers.size(); i++) {
            Marker curr_m = TheMarkers[i];
            if (curr_m.getArea() < 3000)
                continue;
            //TheMarkers[i].calculateExtrinsics(0.034, )
            // cout << TheMarkers[i] << endl;    
            cout << "Marker Rvec rad -> deg : "<<endl << curr_m.Rvec *  180.0 / 3.14159265;
            TheMarkers[i].draw(im, Scalar(0, 0, 255), 1);
            TheMarkers[i].draw(TheInputImageCopy, Scalar(0, 0, 255), 1);
            Point2f c = TheMarkers[i].getCenter();// << endl;
            
            // cout << "M["<<i<<"]: pos 1:" <<curr_m[0].x<<" "<< curr_m[0].y<<" ,center:"<< c.x << ", " << c.y  << " deg " <<get_angle(curr_m[0].x, curr_m[0].y, curr_m[1].x, curr_m[1].y)<< endl;
            //cout << "M.area " << curr_m.getArea() << endl;
            cout << "Marker raw deg " <<get_angle(curr_m[0].x, curr_m[0].y, curr_m[1].x, curr_m[1].y); 
            cout << " | adjusted [" << 180 - get_angle(curr_m[0].x, curr_m[0].y, curr_m[1].x, curr_m[1].y) + theta0 << "]" << endl;
            float ang  = get_angle(cx, cy, c.x, c.y);
            cout << "new theta0 " << get_angle(curr_m[1].x, curr_m[1].y, curr_m[0].x, curr_m[0].y) << endl;
            curl_ang = ang;
            cout << "Thteta0 " << theta0 << endl;
             if(contourArea(max_contour)>6000)
                cout << "ANGLE [" << (int) (180 -ang + theta0)  <<"]" << endl;
            circle(im,Point(c.x, c.y),20,Scalar(255,0,0),-1); // -1 => FILLED circle
            circle(TheInputImageCopy,Point(c.x, c.y),20,Scalar(255,0,0),-1); // -1 => FILLED circle


            if(contourArea(max_contour)>6000){
                curl_should_run = true;
                cout << "curl" << endl;
            }

            
        }
        //if (TheMarkers.size() != 0)
          //  cout << endl;
        AvrgTime.first += ((double)getTickCount() - tick) / getTickFrequency();
        AvrgTime.second++;
        cout << "\r[O.pos] Time detection=" << 1000 * AvrgTime.first / AvrgTime.second << " milliseconds nmarkers=" << TheMarkers.size() << endl;

        imshow("countours", im);
        imshow("in", TheInputImageCopy);
        }
        catch(Exception& e) {
            cerr << "!!!!PANIC PANIC PANIC !!!!!" << endl;
            waitKey(20);
        }
        int k = waitKey(20) & 0xff;
        if (k == 27)
            break;
    }//*/
    curl_kill = true;
    //c.join();
    cap.release();
    destroyAllWindows();
}
