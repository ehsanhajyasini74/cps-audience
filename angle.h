#include <math.h>
#define PI 3.14159265

float get_angle(int x1, int y1, int x2, int y2){
    float x_diff = x2 - x1;
    float y_diff = y2 - y1;
    if(x_diff == 0)
        return 90;
    float param = y_diff / x_diff;
    return atan (param) * 180.0 / PI;
}

float rotation_angle(int xh, int yh, int xm, int ym, float m_an)
{
	float delta_an = get_angle(xh,yh, xm, ym);
	return delta_an - m_an;
}
