#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <vector>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char const *argv[])
{
    cout << "opening RTSP"<<endl;
    VideoCapture cap("rtsp://192.168.0.100/live4.sdp");
    Mat frame;
    cap.read(frame);
    imshow("camera", frame);
    imwrite("1.jpg", frame);
    waitKey(400);
    cap.release();
    destroyAllWindows();
}
